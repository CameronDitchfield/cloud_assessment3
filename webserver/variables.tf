variable "image_id" {
  type = map
  default = {
    eu-west-1 = "ami-0bb3fad3c0286ebd5"
    us-west-2 = "ami-0528a5175983e7f28"
  }
}

variable "tags" {
  default = {
    Name       = " KHC Web Server "
    Start_Date = "2020-11-17"
  }
}

variable "region" {
  type    = string
  default = "us-west-2"
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "keyname" {
  type    = string
  default = "hassan_uw2"
}

variable "workspace" {
  type    = string
  default = "default"
}

variable "env" {
  type = string
}