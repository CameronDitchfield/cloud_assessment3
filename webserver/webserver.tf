#Instances in Public Subnets
resource "aws_instance" "bastion1" {
  ami                    = var.image_id[var.region]
  instance_type          = var.instance_type
  key_name               = var.keyname
  vpc_security_group_ids = [data.terraform_remote_state.infra.outputs.bastion_sg_id, data.terraform_remote_state.infra.outputs.bastion_ghost_sg_id]
  iam_instance_profile   = "S3AccessProfile"
  count                  = 1
  subnet_id              = element(data.terraform_remote_state.infra.outputs.pub_subnet_id, count.index)
  user_data              = <<EOF
        #!/bin/bash
        cat > /usr/bin/connect <<- '_END_'
        #!/bin/bash
        aws s3 cp s3://khc-ass3v2/keypairs/user_khc_${var.workspace}.pem ~/.ssh/user_khc_${var.workspace}.pem
        chmod 600 ~/.ssh/user_khc_${var.workspace}.pem
        ssh -i ~/.ssh/user_khc_${var.workspace}.pem ec2-user@\$1
        rm ~/.ssh/user_khc_${var.workspace}.pem
_END_
        chmod +x /usr/bin/connect
  EOF

  tags = {
    Name = "KHC Bastion Host-${count.index + 1}-${var.workspace}"
  }
}

# resource "aws_instance" "web1" {
#   ami                    = var.image_id[var.region]
#   instance_type          = var.instance_type
#   key_name               = var.keyname
#   vpc_security_group_ids = [data.terraform_remote_state.infra.outputs.khc_sg_id]
#   count                  = 1
#   subnet_id              = element(data.terraform_remote_state.infra.outputs.pub_subnet_id, count.index)
#   user_data              = <<EOF
#         #!/bin/bash
#         yum -y install httpd
#         systemctl enable httpd
#         systemctl start httpd
#         echo "<h1>Welcome to Hassan's Web Server</h1>" >/var/www/html/index.html
#     EOF

#   tags = {
#     Name = "KHC Public Web Server-${count.index + 1}-${var.workspace}"
#   }
# }

#Instances in Private Subnets
resource "aws_instance" "web2" {
  ami                    = var.image_id[var.region]
  instance_type          = var.instance_type
  key_name               = var.keyname
  vpc_security_group_ids = [data.terraform_remote_state.infra.outputs.khc_sg_id]
  count                  = 1
  subnet_id              = element(data.terraform_remote_state.infra.outputs.priv_subnet_id, count.index)
  user_data              = <<EOF
        #!/bin/bash
        yum -y install httpd
        systemctl enable httpd
        systemctl start httpd
        echo "<h1>Welcome to Hassan's Web Server</h1>" >/var/www/html/index.html
    EOF

  tags = {
    Name = "KHC Private Web Server-${count.index + 1}-${var.workspace}"
  }
}