# output "web_pub_dns" {
#   value       = aws_instance.web1.*.public_dns
#   description = "The Public DNS of Instances"
# }

# output "web_pub_id" {
#   value       = aws_instance.web1.*.id
#   description = "The Public IDs of Instances"
# }

output "bastion_pub_dns" {
  value       = aws_instance.bastion1.*.public_dns
  description = "The Public DNS of Instances"
}

output "bastion_pub_id" {
  value       = aws_instance.bastion1.*.id
  description = "The Public IDs of Instances"
}

output "web_priv_dns" {
  value       = aws_instance.web2.*.private_dns
  description = "The Private DNS of Instances"
}

output "web_priv_id" {
  value       = aws_instance.web2.*.id
  description = "The Private IDs of Instances"
}