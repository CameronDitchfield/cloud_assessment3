#!/bin/bash
AWS_ACCESS_KEY=$(grep -A2 '\[academy\]' ~/.aws/credentials | grep -i aws_access | awk '{print $3}' | sed 's/  *//g')
AWS_SECRET_KEY=$(grep -A2 '\[academy\]' ~/.aws/credentials | grep -i aws_secret | awk '{print $3}' | sed 's/  *//g')
AWS_SESSION_TOKEN=$(grep -A3 '\[academy\]' ~/.aws/credentials | grep -i aws_session | awk '{print $3}' | sed 's/  *//g')
export AWS_ACCESS_KEY AWS_SECRET_KEY AWS_SESSION_TOKEN
export AWS_PROFILE=academy
export AWS_REGION=us-west-2
