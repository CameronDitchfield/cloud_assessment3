# Takes two arguments, the environment to launch in , dev or prod and the api token you made
env=$1
token=$2
if [[ -z $env ]] || [[ -z $token ]]
then 
    echo "Requires two arguments, env and token"
    exit 1
else 
    echo "Launching in environment $env"
fi
# token=11ee7cc472aef591e05e578261d3c1ae4e
source ./../set_env.sh
aws s3 cp s3://$bucket_name/jenkins/jenkins_dns.txt jenkins_dns.txt
jenkins_url=$(cat jenkins_dns.txt)
rm jenkins_dns.txt

# Remotely trigger jenkins job to run terraform infra build
# If successful this will trigger the next jenkins job in the chain
curl -X POST http://admin:$token@$jenkins_url/job/launch_infra/buildWithParameters?token=INFRASTRUCTURE