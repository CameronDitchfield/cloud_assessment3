#!/bin/bash
[[ -z $env ]] && env=$1

# Delete loadbalanced petclinic
cd ../petclinic_lb
terraform workspace select $env
if terraform destroy --var-file=$env.tfvars --auto-approve
then 
    echo "Petclinic deleted successfully"
else 
    echo "Petclinic delete failed - cleaning up"
    exit 6
fi

# Delete RDS
cd ../rds
terraform workspace select $env
if terraform destroy --var-file=$env.tfvars --auto-approve
then 
    echo "RDS created successfully"
else 
    echo "RDS build failed - cleaning up"
    exit 4
fi

# Delete Vault
cd ../vault
terraform workspace select $env
if terraform destroy --var-file=$env.tfvars --auto-approve
then 
    echo "Vault created successfully"
else 
    echo "Vault build failed - cleaning up"
    exit 3
fi

# Delete Bastion
cd ../bastion_lb 
terraform workspace select $env
if terraform destroy --var-file=$env.tfvars --auto-approve
then 
    echo "Bastion created successfully"
else 
    echo "Bastion build failed - cleaning up"
    exit 2
fi

# Delete infrastructure
cd ../infra 
terraform workspace select $env
if terraform destroy --var-file=$env.tfvars --auto-approve
then 
    echo "Infrastructure created successfully"
else 
    echo "Infrastructure build failed - cleaning up"
    exit 1
fi