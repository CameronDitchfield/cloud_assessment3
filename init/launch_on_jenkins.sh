#!/bin/bash
env=$1

# Launch infrastructure
cd ../infra 
terraform workspace new $env
terraform workspace select $env
if terraform apply --var-file=$env.tfvars --auto-approve
then 
    echo "Infrastructure created successfully"
else 
    echo "Infrastructure build failed - cleaning up"
    exit 1
fi

# Launch Bastion
cd ../bastion_lb 
terraform workspace new $env
terraform workspace select $env
if terraform apply --var-file=$env.tfvars --auto-approve
then 
    echo "Bastion created successfully"
else 
    echo "Bastion build failed - cleaning up"
    exit 2
fi

# Launch Vault
cd ../vault
terraform workspace new $env
terraform workspace select $env
if terraform apply --var-file=$env.tfvars --auto-approve
then 
    echo "Vault created successfully"
else 
    echo "Vault build failed - cleaning up"
    exit 3
fi

# Launch RDS
cd ../rds
terraform workspace new $env
terraform workspace select $env
if terraform apply --var-file=$env.tfvars --auto-approve
then 
    echo "RDS created successfully"
else 
    echo "RDS build failed - cleaning up"
    exit 4
fi

# Make petclinic AMI
cd ../make_petclinic
if ./launch-ami.sh $env $pemfile
then 
    echo " created successfully"
else 
    echo "RDS build failed - cleaning up"
    exit 5
fi

# Launch loadbalanced petclinic
cd ../petclinic_lb
terraform workspace new $env
terraform workspace select $env
if terraform apply --var-file=$env.tfvars --auto-approve
then 
    echo "Petclinic launched successfully"
else 
    echo "Petclinic launch failed - cleaning up"
    exit 6
fi