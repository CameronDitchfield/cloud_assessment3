#!/bin/bash
# Builds the jenkins machine locally, 
# then launches the terraform job to build the rest of the infrastructure
# Takes one argument, the environment to launch in , dev or prod
env=$1
if [[ $env == "dev" ]] || [[ $env == "prod" ]]
then 
    echo "Building in environment $env"
elif [[ ! -z $env ]]
then
    echo "Require environment argument: init.sh <env>"
    exit 1
else 
    echo "Environment must be 'dev' or 'prod'"
    exit 1
fi

source ./../set_env.sh

# Takes key configuration values from the config file
region=$(cat config | grep region | awk '{print $2}')
bucket_name=$(cat config | grep bucket_name | awk '{print $2}')
aws_accid=$(cat config | grep aws_accid | awk '{print $2}')

# Make s3 bucket
./s3bucket.sh $region $bucket_name $aws_accid 

# Make SSH keys and send them to the s3 bucket
./keys 

# Launch jenkins machine
cd ../ansible
ansible-playbook -i environments/$env launch_jenkins.yml

# Return jenkins dns
aws s3 cp s3://$bucket_name/jenkins/jenkins_dns.txt jenkins_dns.txt
jenkins_url=$(cat jenkins_dns.txt)
echo "The dns of your jenkins server is $jenkins_url"
