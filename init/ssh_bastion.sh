#!/bin/bash

#Connect script to ssh onto the Bastion Host

env=dev
keyname=user_khc_$env
host=bastion-khc.academy.grads.al-labs.co.uk
bucket_name=khc-ass3v2

echo "Connecting to Bastion Host. Please Wait... "
aws s3 cp s3://$bucket_name/keypairs/$keyname.pem ~/.ssh/$keyname.pem
chmod 600 ~/.ssh/$keyname.pem
sleep 5
ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i ~/.ssh/$keyname.pem ec2-user@$host
rm ~/.ssh/$keyname.pem