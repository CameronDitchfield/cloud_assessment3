#!/bin/bash

region=$1
bucket_name=$2
aws_accid=$3
iamrole=242392447408:role/ALAcademyJenkinsSuperRole

export AWS_DEFAULT_REGION=$region 
cat > s3policy.json <<_END
{
    "Version":"2012-10-17",
    "Statement":[{
        "Sid":"AddAWSAccounts",
        "Effect":"Allow",
        "Principal": {"AWS": [
            "arn:aws:iam::$aws_accid:root",
            "arn:aws:iam::$aws_accid:assumed-role/OrganizationAccountAccessRole/hassan@automationlogic.com",
            "arn:aws:iam::$aws_accid:assumed-role/OrganizationAccountAccessRole/cameron@automationlogic.com",
            "arn:aws:iam::$aws_accid:assumed-role/OrganizationAccountAccessRole/kim@automationlogic.com",
            "arn:aws:iam::$iamrole"
        ]},
        "Action":["s3:*"],
        "Resource": [
            "arn:aws:s3:::$bucket_name",
            "arn:aws:s3:::$bucket_name/*"
        ]
        }]
}
_END

if aws s3api create-bucket --bucket $bucket_name \
    --create-bucket-configuration LocationConstraint=$region 
then 
    echo "S3 bucket $bucket_name created successfuly"
else
    echo "S3 bucket creation failed, check the name is unique"
    exit 1
fi 


if aws s3api put-bucket-policy --bucket $bucket_name \
    --policy file://s3policy.json 
then 
    echo "Bucket policy added"
else
    echo "Failed to add policy to bucket"
    exit 2
fi
rm s3policy.json

aws s3api put-public-access-block \
    --bucket $bucket_name \
    --public-access-block-configuration "BlockPublicAcls=true,\
    IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"

aws s3api put-bucket-versioning \
    --bucket $bucket_name \
    --versioning-configuration Status=Enabled

if aws s3api put-bucket-tagging --bucket $bucket_name \
    --tagging TagSet='[{Key=Name,Value=Ass3BucketKHC},
        {Key=Project,Value=ALAcademy2020Oct},
        {Key=Info,Value=s3-for-cloud-infra},
        {Key=Owner,Value=Kim-Hassan-Cameron},
        {Key=Start date,Value=12Nov2020},
        {Key=End date,Value=30Nov2020},
        {Key=Team,Value=Cameron-Hassan-Kim}]'
then 
    echo "Bucket tagged successfully"
else 
    echo "Failed to tag bucket"
    exit 3
fi
