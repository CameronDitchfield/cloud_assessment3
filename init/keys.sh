#!/bin/bash

#Initialisation Script: creates key pairs and moves to S3 bucket
aws ec2 create-key-pair --key-name user_khc_dev | jq -r '.KeyMaterial' >user_khc_dev.pem
chmod 600 user_khc_dev.pem
aws s3 cp user_khc_dev.pem s3://khc-ass3v2/keypairs/user_khc_dev.pem

# aws ec2 create-key-pair --key-name user_khc_prod | jq -r '.KeyMaterial' >user_khc_prod.pem
# chmod 600 user_khc_prod.pem
# aws s3 cp user_khc_prod.pem s3://khc-ass3v2/keypairs/user_khc_prod.pem

# do the terraform builds

# rm the .pem files

