# Public Subnets
resource "aws_subnet" "khc_public" {
  vpc_id                  = aws_vpc.khc_vpc.id
  cidr_block              = var.public_subnets_cidr_blocks[count.index]
  count                   = 2
  availability_zone       = var.azs[count.index]
  map_public_ip_on_launch = true

  tags = {
    Name = "KHC Public Subnet-${count.index + 1}-${var.workspace}"
  }
}

# Route Table
resource "aws_route_table" "khc_public_rt" {
  vpc_id = aws_vpc.khc_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.khc_igw.id
  }

  tags = {
    Name = "KHC Public Route Table-${var.workspace}"
  }
}

# Route table association with public subnets
resource "aws_route_table_association" "khc_public_rta" {
  count          = 2
  subnet_id      = element(aws_subnet.khc_public.*.id, count.index)
  route_table_id = aws_route_table.khc_public_rt.id
}

# Private Subnets
resource "aws_subnet" "khc_private" {
  vpc_id                  = aws_vpc.khc_vpc.id
  cidr_block              = var.private_subnets_cidr_blocks[count.index]
  count                   = 2
  availability_zone       = var.azs[count.index]
  map_public_ip_on_launch = false

  tags = {
    Name = "KHC Private Subnet-${count.index + 1}-${var.workspace}"
  }
}

# Route table association private with vpc main route table
resource "aws_route_table_association" "khc_private_rta" {
  count          = 2
  subnet_id      = element(aws_subnet.khc_private.*.id, count.index)
  route_table_id = aws_vpc.khc_vpc.default_route_table_id
}

# Database subnet group for the RDS instance
resource "aws_db_subnet_group" "khc_rds_snet_grp" {
  name       = "khc_rds_snet_grp"
  subnet_ids = aws_subnet.khc_private.*.id

  tags = {
    Name = "DB subnet group-${var.workspace}"
  }
}