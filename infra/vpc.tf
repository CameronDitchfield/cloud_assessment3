# VPC
resource "aws_vpc" "khc_vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags                 = var.tags
}

# Main route table for private networks 
resource "aws_default_route_table" "r" {
  default_route_table_id = aws_vpc.khc_vpc.default_route_table_id

  tags = {
    Name = "KHC Main VPC Route Table -${var.workspace}"
  }
}

# Internet Gateway
resource "aws_internet_gateway" "khc_igw" {
  vpc_id = aws_vpc.khc_vpc.id

  tags = {
    Name = "KHC IGW -${var.workspace}"
  }
}