variable "vpc_cidr" {
  type    = string
  default = "192.168.0.0/16"
}

variable "tags" {
  default = {
    Name       = " KHC VPC "
    Start_Date = "2020-11-10"
  }
}

variable "hassanip" {
  type    = string
  default = "82.37.149.182/32"
}

variable "kimip" {
  type    = string
  default = "92.12.96.128/32"
}

variable "cameronip" {
  type    = string
  default = "217.155.38.219/32"
}

variable "image_id" {
  type = map
  default = {
    eu-west-1 = "ami-0bb3fad3c0286ebd5"
    us-west-2 = "ami-0528a5175983e7f28"
  }
}

variable "region" {
  type    = string
  default = "us-west-2"
}

variable "azs" {
  type    = list(string)
  default = ["us-west-2a", "us-west-2b", "us-west-2c"]
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "keyname" {
  type    = string
  default = "hassan_uw2"
}

variable "public_subnets_cidr_blocks" {
  type    = list(string)
  default = ["192.168.0.0/18", "192.168.64.0/18"]
}

variable "private_subnets_cidr_blocks" {
  type    = list(string)
  default = ["192.168.128.0/18", "192.168.192.0/18"]
}

variable "workspace" {
  type    = string
  default = "default"
}