#Empty Security Group for Bastion Host - for SSH onto private network
resource "aws_security_group" "KHC_BASTION_GHOST" {
  name        = "KHC_BASTION_GHOST - ${var.workspace}"
  description = "Bastion Security Group"
  vpc_id      = aws_vpc.khc_vpc.id

  tags = {
    Name       = "KHC BastGhost SG - ${var.workspace}"
    Start_Date = "2020-11-10"
  }
}

data "aws_s3_bucket_object" "jenkins_ip" {
  bucket = "khc-ass3v2"
  key    = "jenkins/jenkins_network.txt"
}

#Security Group for SSH onto Bastion - allow only SSH from team
resource "aws_security_group" "KHC_BASTION_SG" {
  name        = "KHC_BASTION_SG - ${var.workspace}"
  description = "Bastion Security Group"
  vpc_id      = aws_vpc.khc_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.hassanip, var.kimip, var.cameronip, var.vpc_cidr, join("/", [chomp(replace(data.aws_s3_bucket_object.jenkins_ip.body, "/  *$/", "")), "32"])]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name       = "KHC Bastian SG - ${var.workspace}"
    Start_Date = "2020-11-10"
  }
}

#Security Group for the database - Allow traffic on VPC subnets to access DB
resource "aws_security_group" "DB_SG" {
  name = "vpc_db"
  description = "DB Security Group"

  ingress {
  from_port = 0
  to_port = 3306
  protocol = "tcp"
  cidr_blocks = [var.hassanip, var.kimip, var.cameronip, var.vpc_cidr]
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = -1
      cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.khc_vpc.id

    tags = {
    Name       = "KHC DB SG - ${var.workspace}"
    Start_Date = "2020-11-19"
  }
}


#Main SecGrp
resource "aws_security_group" "KHC_SG" {
  name        = "Terra_KHC_SG - ${var.workspace}"
  description = "Web Server Security Group"
  vpc_id      = aws_vpc.khc_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.hassanip, var.kimip, var.cameronip]
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = [var.vpc_cidr]
  }

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.KHC_BASTION_GHOST.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name       = "KHC Web SG - ${var.workspace}"
    Start_Date = "2020-11-10"
  }
}