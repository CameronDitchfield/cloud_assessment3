output "pub_subnet_id" {
  value       = aws_subnet.khc_public.*.id
  description = "List of IDs of Public Subnets"
}

output "priv_subnet_id" {
  value       = aws_subnet.khc_private.*.id
  description = "List of IDs of Private Subnets"
}

output "vpc_id" {
  value       = aws_vpc.khc_vpc.id
  description = "The VPC ID created"
}

output "vpc_sg_id" {
  value       = aws_vpc.khc_vpc.default_security_group_id
  description = "The VPC Security Group created"
}

output "khc_sg_id" {
  value       = aws_security_group.KHC_SG.id
  description = "The KHC security group ID"
}

output "bastion_ghost_sg_id" {
  value       = aws_security_group.KHC_BASTION_GHOST.id
  description = "The Bastion ghost security group ID"
}

output "bastion_sg_id" {
  value       = aws_security_group.KHC_BASTION_SG.id
  description = "The Bastion security group ID"
}

output "db_sg_id" {
  value       = aws_security_group.DB_SG.id
  description = "The Database security group ID"
}

output "vpc_default_route_table_id" {
  value       = aws_vpc.khc_vpc.default_route_table_id
  description = "The default route table for VPC"
}

output "aws_internet_gateway_id" {
  value       = aws_internet_gateway.khc_igw.id
  description = "IGW ID"
}

output "rds_subnet_grp_id" {
  value = aws_db_subnet_group.khc_rds_snet_grp.id
  description = "RDS DB subnet group ID"
}