#Outputs from creating LoadBalancers, LaunchConfig and ASG's

output "petclinic-lb-dns" {
  value       = aws_route53_record.www-petclinic.name
  description = "The name of the Petclinic record"
}