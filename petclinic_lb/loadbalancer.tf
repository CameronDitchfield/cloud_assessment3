#Load Balancer for PetClinic
resource "aws_elb" "petclinic_lb" {
  name            = "KHC-LB-PetClinic-${var.workspace}"
  security_groups = [data.terraform_remote_state.infra.outputs.khc_sg_id]
  subnets         = data.terraform_remote_state.infra.outputs.pub_subnet_id

  listener {
    instance_port     = 8080
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8080/"
    interval            = 30
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 120
  connection_draining         = true
  connection_draining_timeout = 120

  tags = {
    Name = "KHC Loadbalancer PetClinic -${var.workspace}"
  }
}

#Launch Configuration for PetClinic
resource "aws_launch_configuration" "laconf_petclinic" {
  name            = "KHC-LaunchConfig-PetClinic-${var.workspace}"
  image_id        = data.terraform_remote_state.pc-im.outputs.pc_image_id
  instance_type   = var.instance_type
  key_name        = var.keyname
  security_groups = [data.terraform_remote_state.infra.outputs.khc_sg_id]
  
  lifecycle {
    create_before_destroy = true
  }
}

#ASG for Petclinic
resource "aws_autoscaling_group" "asg_petclinic" {
  name                      = "khc-asg-petclinic-${var.workspace}"
  max_size                  = 3
  min_size                  = 1
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = 1
  force_delete              = true
  launch_configuration      = aws_launch_configuration.laconf_petclinic.name
  vpc_zone_identifier       = data.terraform_remote_state.infra.outputs.priv_subnet_id
  load_balancers            = [aws_elb.petclinic_lb.name]

  tag {
    key                 = "Name"
    value               = "PC-ASG"
    propagate_at_launch = true
  }

  timeouts {
    delete = "15m"
  }

}

#ASG Attachment for PetClinic
resource "aws_autoscaling_attachment" "asg_attachment_petclinic" {
  autoscaling_group_name = aws_autoscaling_group.asg_petclinic.id
  elb                    = aws_elb.petclinic_lb.id
}

#Route53 for PetClinic - redirects to the petclinic webserver
resource "aws_route53_record" "www-petclinic" {
  zone_id = var.zone_id
  name    = "petclinic-khc.academy.grads.al-labs.co.uk"
  type    = "CNAME"
  ttl     = "60"
  records = [aws_elb.petclinic_lb.dns_name]
}