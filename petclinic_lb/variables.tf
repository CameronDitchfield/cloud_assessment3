variable "region" {
  type    = string
  default = "us-west-2"
}

variable "azs" {
  type    = list(string)
  default = ["us-west-2a", "us-west-2b", "us-west-2c"]
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "keyname" {
  type    = string
  default = ""
}

variable "workspace" {
  type    = string
  default = "default"
}

variable "env" {
  type = string
}

variable "image_id" {
  type = map
  default = {
    eu-west-1 = "ami-0bb3fad3c0286ebd5"
    us-west-2 = "ami-0528a5175983e7f28"
  }
}

variable "zone_id" {
  type    = string
  default = "Z07626429N74Z31VDFLI"
}