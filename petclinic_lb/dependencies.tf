data "terraform_remote_state" "infra" {
  backend = "s3"
  config = {
    bucket = "khc-ass3v2"
    key    = "env:/${var.env}/infra/terraform.tfstate"
    region = var.region
  }
}

data "terraform_remote_state" "pc-im" {
  backend = "s3"
  config = {
    bucket = "khc-ass3v2"
    key    = "env:/${var.env}/pc-im/terraform.tfstate"
    region = var.region
  }
}

data "terraform_remote_state" "rds" {
  backend = "s3"
  config = {
    bucket = "khc-ass3v2"
    key    = "env:/${var.env}/rds/terraform.tfstate"
    region = var.region
  }
}