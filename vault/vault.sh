#!/bin/bash -xv

if (( $# < 1 ))
then
	echo "$0 <env> <key_name>"
	exit 1
fi

source ../set_env.sh
./getssh.sh $1

cd launch
terraform init
if terraform workspace select $1
then
  terraform apply -auto-approve -var-file=${1}.tfvars
else 
  terraform workspace new $1
  terraform apply -auto-approve -var-file=${1}.tfvars
fi
cd ../
# user_khc_dev
rm ${2}.pem 
# if terraform apply -auto-approve -var-file=${1}.tfvars
# then
# 	if terraform destroy -auto-approve -var-file=${1}.tfvars
# 	then
# 		[[ -e ../ansibleprov/environments/dev/hosts ]] && rm ../ansibleprov/environments/dev/hosts
# 	fi
# fi