output "khc_vault_id" {
  value       = aws_instance.vault.*.id
  description = "The id of the KHC Vault instance"
}

output "khc_vault_dns" {
  value       = aws_instance.vault.*.private_dns
  description = "The private dns of the KHC Vault instance"
}