variable "image_id" {
  type = map
  default = {
    eu-west-1 = "ami-0bb3fad3c0286ebd5"
    us-west-2 = "ami-0528a5175983e7f28"
  }
}

variable "region" {
  type    = string
  default = "us-west-2"
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

# GET KEY FROM S3 BUCKET
variable "keyname" {
  type    = string
  default = "cameron_uw2"
}

# variable "keyname" {
#   type    = string
#   default = "user_khc_dev"
# }

variable "pemfile" {
  type = string
}

variable "workspace" {
  type    = string
  default = "default"
}

variable "env" {
  type = string
}

variable "tags" {
  default = {
    Name       = "KHC Vault"
    Start_Date = "2020-11-10"
  }
}