terraform {
  backend "s3" {
    bucket  = "khc-ass3v2"
    key     = "vault/terraform.tfstate"
    region  = "us-west-2"
    encrypt = true
  }
}