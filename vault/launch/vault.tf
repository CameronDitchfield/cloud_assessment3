# Get jenkins info file from s3 bucket
data "aws_s3_bucket_object" "jenkins_dns" {
  bucket = "khc-ass3v2"
  key    = "jenkins/jenkins_dns.txt"
}

# Add jenkins to inventory
resource "null_resource" "jenkins_inventory" {
  provisioner "local-exec" {
    command = "echo '[jenkins]\n${chomp(replace(data.aws_s3_bucket_object.jenkins_dns.body, "/  *$/", ""))}' >hosts"
    working_dir = "../provision/environments/${var.env}"
  }
}

# data "aws_s3_bucket_object" "ssh_key" {
#   bucket = "khc-ass3v2"
#   key    = "keypairs/user_khc_dev.pem"
# }

# resource "null_resource" "jenkins_ssh_key" {
#   provisioner "local-exec" {
#     command "cp"
#   }
# }

resource "aws_instance" "vault" {
  ami                    = var.image_id[var.region]
  instance_type          = var.instance_type
  key_name               = var.keyname
  vpc_security_group_ids = [data.terraform_remote_state.infra.outputs.khc_sg_id]
  subnet_id              = element(data.terraform_remote_state.infra.outputs.priv_subnet_id, 1)
  tags = {
    Name = "Vault Server-${var.env}"
  }

  #Hack to make terraform wait till the machine is ready for SSH before moving on
  # provisioner "remote-exec" {
  #   inline = ["/usr/bin/hostname"]

  #   connection {
  #     type        = "ssh"
  #     user        = "ec2-user"
  #     private_key = file(var.pemfile)
  #     host        = self.private_dns
  #   }
  # }
}

# Sleep for a minute to allow instance to start
resource "null_resource" "delay" {
  count      = var.env == "dev" ? 1 : 0
  depends_on = [null_resource.run_ansible]
  provisioner "local-exec" {
    command = "sleep 60"
  }
}

# Add instance to ansible inventory
resource "null_resource" "vault_inventory" {
  depends_on = [aws_instance.vault]
  provisioner "local-exec" {
    command     = "echo '[vault]\n${aws_instance.vault.private_ip}' >>hosts"
    working_dir = "../provision/environments/${var.env}"
  }
}

# Run ansible provision on instance
resource "null_resource" "run_ansible" {
  depends_on = [null_resource.vault_inventory]
  provisioner "local-exec" {
    command     = "ansible-playbook -i environments/${var.env} vault.yml"
    working_dir = "../provision"
  }
}

# # Check the webserver can be curled
# resource "null_resource" "check_instance" {
#   depends_on = [null_resource.delay]
#   provisioner "local-exec" {
#     command = "curl -s ${aws_instance.petclinic.public_dns}:8080"
#   }
# }

# Remove instance from ansible inventory
# resource "null_resource" "delete_inventory" {
#   depends_on = [null_resource.check_instance]
#   provisioner "local-exec" {
#     command     = "echo '' >hosts"
#     working_dir = "../environments/${var.env}"
#   }
# }