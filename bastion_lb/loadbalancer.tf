#Load balancer for Bastion
resource "aws_elb" "bastion_lb" {
  name            = "KHC-LB-Bastion-${var.workspace}"
  security_groups = [data.terraform_remote_state.infra.outputs.khc_sg_id]
  subnets         = data.terraform_remote_state.infra.outputs.pub_subnet_id

  listener {
    instance_port     = 22
    instance_protocol = "tcp"
    lb_port           = 22
    lb_protocol       = "tcp"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:22"
    interval            = 30
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 120
  connection_draining         = true
  connection_draining_timeout = 120

  tags = {
    Name = "KHC Loadbalancer Bastion -${var.workspace}"
  }
}

#Launch Configuration for Bastion
resource "aws_launch_configuration" "laconf_bastion" {
  name                 = "KHC-LaunchConfig-Bastion-${var.workspace}"
  image_id             = var.image_id[var.region]
  instance_type        = var.instance_type
  iam_instance_profile = "S3AccessProfile"
  key_name             = var.keyname
  security_groups      = [data.terraform_remote_state.infra.outputs.bastion_sg_id, data.terraform_remote_state.infra.outputs.bastion_ghost_sg_id]
  user_data              = <<EOF
        #!/bin/bash
        yum -y install mysql
        cat > /usr/bin/connect <<- '_END_'
        #!/bin/bash
        PS3="Please choose the service you would like to connect to: "
        select service in PetClinic Database
        do
        case $service in
            PetClinic)
                aws s3 cp s3://khc-ass3v2/keypairs/user_khc_${var.workspace}.pem ~/.ssh/user_khc_${var.workspace}.pem
                chmod 600 ~/.ssh/user_khc_${var.workspace}.pem
                echo "Connecting to $service... "
                sleep 2
                ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i ~/.ssh/user_khc_${var.workspace}.pem ec2-user@$petclinic-khc.academy.grads.al-labs.co.uk
                rm ~/.ssh/user_khc_${var.workspace}.pem
                ;;
            Database)
                echo "Connecting to $service... "
                sleep 2
                mysql -h khc-rds.cbwwctw553uk.us-west-2.rds.amazonaws.com -u petclinic -pcloudvet
                ;;
            *)
                echo "Invalid Option. Please try again."
                break
                ;;
        esac
        done
_END_
        chmod +x /usr/bin/connect
  EOF

  lifecycle {
    create_before_destroy = true
  }
}

#ASG for Bastion
resource "aws_autoscaling_group" "asg_bastion" {
  name                      = "khc-asg-bastion-${var.workspace}"
  max_size                  = 3
  min_size                  = 1
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = 1
  force_delete              = true
  launch_configuration      = aws_launch_configuration.laconf_bastion.name
  vpc_zone_identifier       = data.terraform_remote_state.infra.outputs.pub_subnet_id
  load_balancers            = [aws_elb.bastion_lb.name]

  tag {
    key                 = "Name"
    value               = "BH-ASG"
    propagate_at_launch = true
  }

  timeouts {
    delete = "15m"
  }

}

#ASG Attachment for Bastion
resource "aws_autoscaling_attachment" "asg_attachment_bastion" {
  autoscaling_group_name = aws_autoscaling_group.asg_bastion.id
  elb                    = aws_elb.bastion_lb.id
}

#Route53 for Bastian - redirects ssh to the bastion instance
resource "aws_route53_record" "www-bastion" {
  zone_id = var.zone_id
  name    = "bastion-khc.academy.grads.al-labs.co.uk"
  type    = "CNAME"
  ttl     = "60"
  records = [aws_elb.bastion_lb.dns_name]
}
