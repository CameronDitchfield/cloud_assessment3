#Outputs from creating LoadBalancers, LaunchConfig and ASG's

output "bastion-lb-dns" {
  value       = aws_route53_record.www-bastion.name
  description = "The name of the Bastion record"
}