env=dev
keyname=user_khc_dev
host=bastion-khc.academy.grads.al-labs.co.uk
bucket_name=khc-ass3v2

aws s3 cp s3://$bucket_name/keypairs/$keyname.pem ~/.ssh/$keyname.pem
chmod 600 ~/.ssh/$keyname.pem

cat > test_remote.sh <<-'EOF'
  mysql --host=khc-rds.cbwwctw553uk.us-west-2.rds.amazonaws.com --port=3306 --user=petclinic --password=cloudvet < /tmp/schema.sql
  mysql --host=khc-rds.cbwwctw553uk.us-west-2.rds.amazonaws.com --port=3306 --user=petclinic --password=cloudvet < /tmp/data.sql
EOF

scp -o "StrictHostKeyChecking=no" -i ~/.ssh/$keyname.pem *.sql ec2-user@$host:/tmp/
ssh -o "StrictHostKeyChecking=no" -i ~/.ssh/$keyname.pem ec2-user@$host 'bash -s' < test_remote.sh
rm ~/.ssh/$keyname.pem