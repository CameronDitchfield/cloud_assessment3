variable "tags" {
  default = {
    Name       = " KHC RDS "
    Start_Date = "2020-11-10"
  }
}

variable "env" {
  type = string
}

variable "region" {
  type    = string
  default = "us-west-2"
}

variable "keyname" {
  type    = string
  default = ""
}

variable "workspace" {
  type    = string
  default = "default"
}

variable "bastion_dns" {
  type = string
  default = "bastion-khc.academy.grads.al-labs.co.uk"
}