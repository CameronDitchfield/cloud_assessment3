#!/bin/bash

if (( $# < 1 ))
then
	echo "$0 <env>"
	exit 1
fi

source ../set_env.sh

terraform init
if terraform workspace select $1
then
  terraform apply -auto-approve -var-file=${1}.tfvars
else 
  terraform workspace new $1
  terraform apply -auto-approve -var-file=${1}.tfvars
fi
