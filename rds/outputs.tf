output "khc_rds_id" {
  value       = aws_db_instance.khc_rds.id
  description = "The id of the khc_rds instance"
}

output "khc_rds_endpoint" {
  value       = aws_db_instance.khc_rds.endpoint
  description = "The endpoint of the khc_rds instance"
}