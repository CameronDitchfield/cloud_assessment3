resource "aws_db_instance" "khc_rds" {
  allocated_storage = 8
  storage_type      = "gp2"
  instance_class    = "db.t2.micro"
  engine            = "mysql"
  engine_version    = "8.0.20"
  identifier        = "khc-rds"
  name              = "khc_db"

  # provisioner "local-exec" {
  #   interpreter = ["/bin/bash"]
  #   command = <<-EOF
  #     # SET PASSWORD TO RESULT OF GET PASSWORD SCRIPT
  #   EOF
  # }
  username = "petclinic"
  password = "cloudvet"

  apply_immediately         = true
  final_snapshot_identifier = "khc-rds-snapshot"
  skip_final_snapshot       = true
  multi_az                  = true
  db_subnet_group_name      = data.terraform_remote_state.infra.outputs.rds_subnet_grp_id
  vpc_security_group_ids    = [data.terraform_remote_state.infra.outputs.db_sg_id]

  provisioner "local-exec" {
    interpreter = ["/bin/bash"]
    command     = <<-EOF
      #!/bin/bash -xv
      aws s3 cp s3://khc-ass3v2/keypairs/${var.keyname}.pem ${var.keyname}.pem
      chmod 600 ${var.keyname}.pem

      scp -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i ${var.keyname}.pem *.sql ec2-user@${var.bastion_dns}:/home/ec2-user/

      cat > populate_db_remote.sh <<- '_END_'
        mysql -h ${self.address} -u ${self.username} -p${self.password} < schema.sql
        mysql -h ${self.address} -u ${self.username} -p${self.password} < data.sql
_END_

      chmod +x populate_db_remote.sh

      scp -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i ${var.keyname}.pem populate_db_remote.sh ec2-user@${var.bastion_dns}:/home/ec2-user/

      ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i ${var.keyname}.pem ec2-user@${var.bastion_dns} './populate_db_remote.sh'
      rm ${var.keyname}.pem
    EOF
  }
}