data "terraform_remote_state" "infra" {
  backend = "s3"
  config = {
    bucket = "khc-ass3v2"
    key    = "env:/${var.env}/infra/terraform.tfstate"
    region = var.region
  }
}

# data "terraform_remote_state" "vault" {
#   backend = "s3"
#   config = {
#     bucket = "khc-ass3v2"
#     key    = "env:/${var.env}/vault/terraform.tfstate"
#     region = var.region
#   }
# }