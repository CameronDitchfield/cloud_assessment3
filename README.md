# Cloud Assessment 3


## Installation
* Check out the repository into your home directory.

    * git clone https://bitbucket.org/CameronDitchfield/cloud_assessment3.git


* PLEASE EDIT THE CONFIGURATION FILE WITH USER PARAMETERS:
    
    * The user must edit the initial parameters such as AWS Account ID and Region. 

* Execute the *init.sh* script from directly inside the *cloud_assessment3* folder by the following on the CLI (command line interface):
* IMPORTANT: Please ensure the user has downloaded their most recent aws 'credentials' file on their local machine, as this is required to set-up the working environment. If an error occurs during this step, please ensure that the aws 'credentials' is locally downloaded and then run the *set_env.sh* script to ensure that the environment is appropriately configured.

    * ./init/init.sh <env>     *Requires one arguement, environment variable options: dev = development platform environment , prod = production platform environment
* Once the jenkins server is up and running log on to it using the DNS outputted by the init.sh script in order to make a jenkins API token
    * Once on the jenkins server log in with the credentials 
        * Username: admin 
        * Password: atest
    * Navigate to the 'Manage Jenkins' tab, then 'Manage Users', select the admin user and navigate to 'Configure' on the sidebar.
    * In the API Token section, select create new token, give your token a name and take a copy of the token created.
* Now return to the command line in the init directory and run the *launch.sh* script
    * This script requires two parameters;
        *  The environment to build in 'dev' or 'prod'
        *  The API token you created in the previous step
   *  This script will trigger the launch of the infrastructure, which will trigger the launch of each of the individual componenets of the environment.


## Connecting to Bastion Host - Management

* The management user can ssh onto the Bastion Host to further ssh onto the resources within the VPC to perform any required management tasks.

* Firstly, ensure that initialisation has been carried out by entering the AWS account number, so that the user can be granted acces to the S3 bucket and retrieve sensitive credentials.

* Ensure the following parameters are set and exported to the environment.
    
    * export AWS_REGION=<region>            default=us-west-2
    * export AWS_PROFILE=<profile_name>     default=academy

* To ssh onto the Bastian Host, then simply run the 'ssh_bastion.sh' script or by running the following command from the CLI and staying in the home directory. This will directly connect the user to the BAstion Host regardless of the deployment location.

    * ./cloud_assessment3/init/ssh_bastion.sh

* To exit from the Bastion Host simply write 'exit' on the CLI.

## Connecting from Bastion Host to another resource

* The Bastion Host provides a platform for the management user to ssh or connect to another resource within the VPC to perform management or admin activities.

* The user can connect to the following resources:
    
    * "PetClinic" -> the instance running behind the petclinic webserver
    * "Database"  -> the user can log into the petclinic database server
    * "Vault"     -> the instance running behind the vault server

* To connect to one of the resources the user simply inputs on the CLI:

    * connect <resource>        

* To exit from a resource simply write 'exit' on the CLI which would return the user to the Bastion Host.