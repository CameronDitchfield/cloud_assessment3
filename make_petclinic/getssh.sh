#!/bin/bash 
env=$1
aws s3 cp  s3://khc-ass3v2/keypairs/user_khc_dev.pem user_khc_dev.pem
chmod 600 user_khc_dev.pem
keypath=$PWD/user_khc_dev.pem

cat > tf-pc-inst/dev.tfvars <<_END_
env     = "dev"
tags = {
  Team  = "Cameron-Hassan-Kim"
  Environment = "dev"
}
pemfile = "$keypath"
_END_


cat > environments/$env/group_vars/all <<_END_
ansible_ssh_private_key_file: "$keypath"
ansible_ssh_common_args: "-o StrictHostKeyChecking=no -o ProxyCommand='ssh -W %h:%p ec2-user@bastion-khc.academy.grads.al-labs.co.uk'"
ansible_ssh_user: ec2-user
region: us-west-2
ec2_access_key: "{{ lookup('env', 'AWS_ACCESS_KEY') }}"
ec2_secret_key: "{{ lookup('env', 'AWS_SECRET_KEY') }}"
_END_
