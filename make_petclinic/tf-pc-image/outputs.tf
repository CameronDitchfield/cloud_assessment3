output "pc_image_id" {
  value       = var.env == "dev" ? aws_ami_from_instance.pc_ami[0].id : aws_ami_copy.pc_prod_ami[0].id
  description = "The id of the petclinic AMI in this environment"
}
