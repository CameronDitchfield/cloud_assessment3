data "terraform_remote_state" "pc_inst" {
  backend = "s3"
  config = {
    bucket = var.bucketname
    key    = "env:/${var.env}/pc-inst/terraform.tfstate"
    region = var.region
  }
}

data "terraform_remote_state" "pc_im_dev" {
  backend = "s3"
  config = {
    bucket = var.bucketname
    key    = "env:/dev/pc-im/terraform.tfstate"
    region = var.region
  }
}

data "terraform_remote_state" "infra" {
  backend = "s3"
  config = {
    bucket = var.bucketname
    key    = "env:/${var.env}/infra/terraform.tfstate"
    region = var.region
  }
}