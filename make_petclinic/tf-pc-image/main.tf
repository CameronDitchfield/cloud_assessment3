# Create AMI if in dev environment
resource "aws_ami_from_instance" "pc_ami" {
  count              = var.env == "dev" ? 1 : 0
  name               = "petclinic-${var.env}"
  source_instance_id = data.terraform_remote_state.pc_inst.outputs.pc_id
  tags               = merge(var.tags, { Name = var.projectname })
}

# Copy AMI from dev to prod if ij prod environment chosen
resource "aws_ami_copy" "pc_prod_ami" {
  count             = var.env == "prod" ? 1 : 0
  name              = "petclinic-${var.env}"
  description       = "A copy of ${data.terraform_remote_state.pc_im_dev.outputs.pc_image_id}"
  source_ami_id     = data.terraform_remote_state.pc_im_dev.outputs.pc_image_id
  source_ami_region = var.region
  tags              = merge(var.tags, { Name = var.projectname })
}
