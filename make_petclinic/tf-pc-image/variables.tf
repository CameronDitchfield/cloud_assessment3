variable "region" {
  type    = string
  default = "us-west-2"
}

variable "projectname" {
  type    = string
  default = "petclinic"
}

variable "env" {
  type = string
}

variable "tags" {
  default = {
    Team  = "Cameron-Hassan-Kim"
    Environment = "dev"
  }
}

variable "bucketname" {
  type    = string
  default = "khc-ass3v2"
}