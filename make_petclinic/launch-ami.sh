#!/bin/bash 
[[ -z $env ]] && env=$1
./getssh.sh $env
cd tf-pc-inst
terraform init
terraform workspace new $env
terraform workspace select $env
if terraform apply --var-file=$env.tfvars --auto-approve
then
    rm user_khc_dev.pem
    if [[ $env == "dev" ]]
    then 
        echo "Petclinic instance successful, creating image"
    fi
    cd ../tf-pc-image 
    terraform init
    terraform workspace new $env
    terraform workspace select $env
    if terraform apply --var-file=$env.tfvars --auto-approve
    then 
        if [[ $env == "dev" ]]
        then 
            echo "New petclinic AMI created successfully"
        elif [[ $env == "prod" ]]
        then 
            echo "Updated AMI copied to production successfully"
        fi 
    else 
        if [[ $env == "dev" ]]
        then 
            echo "AMI creation failed - cleaning up"
        elif [[ $env == "prod" ]]
        then 
            echo "AMI copy failed"
        fi 
    fi 
    cd ../tf-pc-inst 
    terraform workspace select $env
    terraform destroy --var-file=$env.tfvars  --auto-approve
else 
    rm user_khc_dev.pem
    echo "Petclinic install failed - cleaning up"
    terraform destroy --var-file=$env.tfvars --auto-approve
    exit 1
fi 
