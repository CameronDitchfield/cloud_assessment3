output "pc_id" {
  value       = var.env == "dev" ? aws_instance.petclinic[0].id : "None"
  description = "The id of the provisioned petclinic instance"
}