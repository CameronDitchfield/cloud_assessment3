env     = "dev"
tags = {
  Team  = "Cameron-Hassan-Kim"
  Environment = "dev"
}
db_endpoint = "petclinic.cbwwctw553uk.us-west-2.rds.amazonaws.com"
subnet_ids = ["subnet-c47c998e"]
sg_ids = ["sg-0b44decd56c3d11b3"]
keyname = "kimOregon"
pemfile = "/Users/kimspijkers-shaw/.ssh/kimOregon.pem" 