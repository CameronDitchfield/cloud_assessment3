variable "image_id" {
  type = map
  default = {
    eu-west-1 = "ami-0bb3fad3c0286ebd5"
    us-west-2 = "ami-0528a5175983e7f28"
  }
}

variable "region" {
  type    = string
  default = "us-west-2"
}

variable "azs" {
  type    = list(string)
  default = ["us-west-2a", "us-west-2b", "us-west-2c"]
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "keyname" {
  type    = string
  default = "user_khc_dev"
}

# variable "sg_ids" {
#   type    = list(string)
# }

# variable "subnet_ids" {
#   type    = list(string)
# }

variable "projectname" {
  type    = string
  default = "petclinic"
}

variable "bucketname" {
  type    = string
  default = "khc-ass3v2"
}

variable "iam_profile" {
  type    = string
  default = "s3ReadOnly"
}

variable "env" {
  type = string
}

variable "pemfile" {
  type = string
}

variable "tags" {
  default = {
    Team  = "Cameron-Hassan-Kim"
    Environment = "dev"
  }
}

# variable "vault_dns" {
#   type    = string
# }

# variable "db_endpoint" {
#   type    = string
# }