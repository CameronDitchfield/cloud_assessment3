# Make instance
resource "aws_instance" "petclinic" {
  count                  = var.env == "dev" ? 1 : 0
  ami                    = var.image_id[var.region]
  instance_type          = var.instance_type
  key_name               = var.keyname
  vpc_security_group_ids = [data.terraform_remote_state.infra.outputs.khc_sg_id]
  subnet_id              = element(data.terraform_remote_state.infra.outputs.pub_subnet_id, count.index)
  iam_instance_profile   = var.iam_profile
  tags                   = merge(var.tags, { Name = var.projectname })

  # Hack to make terraform wait till the machine is ready for SSH before moving on
  # provisioner "remote-exec" {
  #   inline = ["/usr/bin/hostname"]

  #   connection {
  #     type        = "ssh"
  #     user        = "ec2-user"
  #     private_key = file(var.pemfile)
  #     host        = self.public_dns
  #   }
  # }
} 

# Sleep for a minute to allow petclinic to start
resource "null_resource" "delay1" {
  count      = var.env == "dev" ? 1 : 0
  depends_on = [aws_instance.petclinic]
  provisioner "local-exec" {
    command = "sleep 60"
  }
}

# Add instance to ansible inventory
resource "null_resource" "create_inventory" {
  count      = var.env == "dev" ? 1 : 0
  depends_on = [null_resource.delay1]
  provisioner "local-exec" {
    command     = "echo '[petclinic]\n${aws_instance.petclinic[0].public_dns}' >hosts"
    working_dir = "../environments/${var.env}"
  }
}

# Run ansible provision on instance
resource "null_resource" "run_ansible" {
  count      = var.env == "dev" ? 1 : 0
  depends_on = [null_resource.create_inventory] #, null_resource.get_jar]
  provisioner "local-exec" {
    command     = "ansible-playbook -i environments/${var.env} petclinic-prov.yml --extra-vars 'dbip=khc-rds.cbwwctw553uk.us-west-2.rds.amazonaws.com'"
    working_dir = "../"
  }
}
#${data.terraform_remote_state.rds.outputs.khc_rds_endpoint}
#vault_dns=${data.terraform_remote_state.vault.outputs.vault_dns},dbip=${data.terraform_remote_state.rds.outputs.khc_rds_endpoint},,pemfile=${var.pemfile}
# Sleep for a minute to allow petclinic to start
resource "null_resource" "delay" {
  count      = var.env == "dev" ? 1 : 0
  depends_on = [null_resource.run_ansible]
  provisioner "local-exec" {
    command = "sleep 60"
  }
}

# Check the webserver can be curled
resource "null_resource" "check_instance" {
  count      = var.env == "dev" ? 1 : 0
  depends_on = [null_resource.delay]
  provisioner "local-exec" {
    command = "curl -s ${aws_instance.petclinic[0].public_dns}:8080"
  }
}

# Remove instance from ansible inventory
resource "null_resource" "delete_inventory" {
  count      = var.env == "dev" ? 1 : 0
  depends_on = [null_resource.check_instance]
  provisioner "local-exec" {
    command     = "echo '' >hosts"
    working_dir = "../environments/${var.env}"
  }
}